var rager_now = Math.floor((new Date).getTime()/1000)
var newReddit = document.querySelector('#SHORTCUT_FOCUSABLE_DIV') !== null;

const process_oldReddit_items = async (items) => {
    let author_ids = $(items).map(function () {
        return $(this).attr("data-author-fullname");
    }).get();

    await applyAuthorData(author_ids)
    for (let t of $('.tagline time')) {
        let itemDate = Math.floor(new Date(t.getAttribute('title')).getTime()/1000)
        let diff = rager_now-itemDate
        if (diff > 86400) {
            let pretty_age = convertSecondsToReadableTime(diff) + ' ago';
            let clone = $(t).clone()
            $(clone).attr('class','').html(pretty_age);
            $(t).after(clone);
            $(t).remove();
        }
    }
}

function process_newReddit() {
    window.fetch(document.URL)
        .then(data => data.text())
        .then(html => {
            var matcher = html.match(/window\.___r = ([\s\S]*);</);
            if (matcher && matcher.length > 1) {
                json = JSON.parse(matcher[1]);
                var comments = json.features.comments.models;
                var posts = json.posts.models;
                var items = Object.assign({}, comments, posts)
                process_newReddit_items(items)
            }
        })
}


const process_newReddit_items = async (items) => {
    author_ids = Object.keys(items).map(k => items[k].authorId)
    await applyAuthorData(author_ids)
    $.each(items, function(id, item) {
        let created = item.created
        if (created > rager_now) {
            created = created / 1000;
        }
        let pretty_time = convertSecondsToReadableTime(rager_now-created)+' ago';
        $(`a[id*="${id}"]`).html(pretty_time);
        $(`a[id$="${id}inOverlay]"`).html(pretty_time);
        // new reddit overlays don't work anymore b/c monitor-requests.js doesn't work
        // arrive.js might fix it
        if (item.id.substr(0,2) === 't3' && item.id.length < 20) {
          $('#'+item.id).find('[data-click-id="timestamp"]').html(pretty_time);
        }
    })
}

function applyAuthorData(ids) {
    return getAuthorData(ids).then(authorDict => {
        $.each(authorDict, function(id, author) {
            let pretty_age = convertSecondsToReadableTime(rager_now-author.created_utc);
            let karma = addCommas(author.link_karma + author.comment_karma);
            let hClass="rager-info"
            if (newReddit) hClass += " rager-redesign"
            $(`a[href$="/user/${author.name}"], a[href$="/user/${author.name}/"]`).each(function(idx, element) {
                if ( (!$(element).next() || ! $(element).next().hasClass('rager-info')) &&
                     $(element).text().match(author.name)) {
                    $(element).after(`<span class="${hClass}">[${pretty_age} | ${karma}]</span>`);
                }
            })
        });
        return authorDict
    });
}

const getAuthorData = async (ids_with_duplicates) => {
    if (ids_with_duplicates && ids_with_duplicates.length) {
        const ids = [...new Set(ids_with_duplicates)]
        return getAuth()
        .then(async (auth) => {
            const results = []
            for (const [i, ids_chunk] of chunk(ids, 500).entries()) {
                // if (i !== 0) {
                //     await new Promise(r => setTimeout(r, 2000))
                // }
                const result = await window.fetch(`https://oauth.reddit.com/api/user_data_by_account_ids.json?ids=${ids_chunk.join(',')}`, auth)
                  .then(data => data.json())
                results.push(result)
            }
            return flatten(results)[0] || {}
        })
    }
    return {}
}

// Split array into chunks of given size
const chunk = (arr, size) => {
  const chunks = []
  for (let i = 0; i < arr.length; i += size) {
    chunks.push(arr.slice(i, i + size))
  }
  return chunks
}

// Flatten arrays to one level
const flatten = arr => arr.reduce(
  (accumulator, value) => accumulator.concat(value),
  []
)


function convertSecondsToReadableTime(seconds) {
    var thresholds = [[60, 'second', 'seconds'], [60, 'minute', 'minutes'], [24, 'hour', 'hours'], [7, 'day', 'days'],
                     [365/12/7, 'week', 'weeks'], [12, 'month', 'months'], [10, 'year', 'years'],
                     [10, 'decade', 'decades'], [10, 'century', 'centuries'], [10, 'millenium', 'millenia']];
    var time = seconds;

    for (var i=0; i<thresholds.length; i++) {
        var divisor = thresholds[i][0];
        var text = thresholds[i][1];
        var textPlural = thresholds[i][2];
        if (time < divisor) {
            var extra = (time - Math.floor(time));
            var prevUnitTime = Math.round(extra*thresholds[i-1][0]);
            if (Math.floor(time) > 1 || Math.floor(time) == 0) {
                text = textPlural;
            }
            if (i > 1 && prevUnitTime > 0) {
                var remainText = thresholds[i-1][1];
                if (prevUnitTime > 1) {
                    remainText = thresholds[i-1][2];
                }
                text += ', ' + String(prevUnitTime) + ' ' + remainText;
            }
            return String(Math.floor(time)) + ' ' + text;
        }
        time = time / divisor;
    }
}

function addCommas (x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
