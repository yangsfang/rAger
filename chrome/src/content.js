let jq = chrome.runtime.getURL('/lib/jquery-3.2.1.min.js');
let auth = chrome.runtime.getURL('/src/auth.js');
let common = chrome.runtime.getURL('/src/common.js');
let monitor = chrome.runtime.getURL('/src/monitor-requests.js');



var newItems = [];
var calledGatherFunc = false;

var newReddit = document.querySelector('#SHORTCUT_FOCUSABLE_DIV') !== null;
if (! newReddit) {
    process_oldReddit_items($('.thing'));
    $(document).arrive(".thing", function() {
        newItems.push(this);
        setTimeout(function () {
            if (! calledGatherFunc) {
                calledGatherFunc = true;
                process_oldReddit_items(newItems);
                setTimeout(function () {
                    calledGatherFunc = false;
                    newItems = [];
                }, 500);
            }
        }, 1000);

    });
} else {
    $(`<script type="text/javascript" src="${jq}"></script>`).appendTo('head');
    $(`<script type="text/javascript" src="${auth}"></script>`).appendTo('head');
    $(`<script type="text/javascript" src="${common}"></script>`).appendTo('head');
    $(`<script type="text/javascript" src="${monitor}"></script>`).appendTo('head');

    process_newReddit();
}


