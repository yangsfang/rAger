# rAger

A chrome extension for displaying reddit account age and karma on comment/post listings without needing to hover over the username.

chrome web store link: https://chrome.google.com/webstore/search/%22rdevcoder%22%20%22rager%22
